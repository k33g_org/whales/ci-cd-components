# ci-cd-components for 🐳 Docker

## How to use it

### Docker Build

```yaml
include:
  - component: gitlab.com/k33g_org/whales/ci-cd-components/kaniko-build@0.0.0
    inputs:
      stage: build
```

### Docker Scout

```yaml
include:
  - component: gitlab.com/k33g_org/whales/ci-cd-components/docker-scout-main@0.0.2
    inputs:
      stage: test
      only-severity: "critical"
  - component: gitlab.com/k33g_org/whales/ci-cd-components/docker-scout-mr@0.0.2
    inputs:
      stage: test
      only-severity: "critical,high"
```


### Docker Build + Docker Scout

```yaml
include:
  - component: gitlab.com/k33g_org/whales/ci-cd-components/kaniko-build@0.0.0
    inputs:
      stage: build
  - component: gitlab.com/k33g_org/whales/ci-cd-components/docker-scout-main@0.0.2
    inputs:
      stage: test
      only-severity: "critical"
  - component: gitlab.com/k33g_org/whales/ci-cd-components/docker-scout-mr@0.0.2
    inputs:
      stage: test
      only-severity: "critical,high"
```

- 📝 GitLab CI/CD components: https://docs.gitlab.com/ee/ci/components/index.html

